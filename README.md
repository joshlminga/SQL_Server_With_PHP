1: Install Xampp
	- https://www.apachefriends.org/download.html

2: Download and Install SQL Server
	- https://www.microsoft.com/en-us/sql-server/sql-server-downloads-free-trial

3: Solve Port conflict issues, and once they all run perfectly continue to step 4
4: Download SQL server PHP drivers
	- https://github.com/Microsoft/msphpsql/releases
	  OR 
	  https://github.com/Microsoft/msphpsql/releases/download/4.1.4-Windows/7.1.zip

5: Run as Administrator and extract to folder
	- Copy the latest drivers according to your System architecture
	- Use php_pdo_sqlsrv_71_ts.dll and php_sqlsrv_71_ts.dll 
		ELSE
	  Use php_pdo_sqlsrv_71_ts.dll and php_sqlsrv_71_ts.dll in x64 Folder

6: Copy the two drivers and paste them in
	- C:\xampp\php\ext

7: Open PHP.INI
   Find extension=php_bz2.dll
   	or search 'extension='  until you find it
   	- Add two lines below it 'extension=php_bz2.dll'
   	* extension=php_pdo_sqlsrv_71_ts.dll *
   	* extension=php_sqlsrv_71_ts.dll *

   	since am runing PHP 7.1

   - Save and Restart Apache
   N.B: If it refuse to open then look at your drivers extension if copied correctly | if failed then the drivers are not supported, copy the other.

8: Also if you update Visual C++ & ODBC drivers
   Download and install
   - Microsoft Visual C++ 2015 Redistributable (x86)
     https://www.microsoft.com/en-us/download/details.aspx?id=48145

   - The ODBC drivers
     https://www.microsoft.com/en-us/download/details.aspx?id=36434


9: Find the PHP script to connect to your SQL server
	- http://php.net/manual/en/function.sqlsrv-connect.php

	*** I checkd which port SQLSERVER is using and found is 1433 ***


	$serverName = "serverName\instanceName, 1433"; //serverName\instanceName, portNumber (default is 1433)
	$connectionInfo = array( "Database"=>"databasename", "UID"=>"username", "PWD"=>"password");
	$conn = sqlsrv_connect( $serverName, $connectionInfo);

	if($conn) {
	     echo "Connection established.<br />";
	}else{
	     echo "Connection could not be established.<br />";
	     die( print_r( sqlsrv_errors(), true));
	}

	N.B:
	
	- Get Server Name from SQL login dashboard or in CMD by typing * hostname *
	- Get Instance Name By Quring from the selected Database * SELECT @@servicename *

10: Test the script if it connect